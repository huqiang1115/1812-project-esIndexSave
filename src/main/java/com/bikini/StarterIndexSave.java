package com.bikini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterIndexSave {
	
	public static void main(String[] args) {
		SpringApplication.run(StarterIndexSave.class, args);
	}
}
