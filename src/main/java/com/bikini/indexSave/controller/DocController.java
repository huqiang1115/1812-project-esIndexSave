package com.bikini.indexSave.controller;

import java.util.List;

import org.elasticsearch.client.transport.TransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bikini.common.pojo.Product;
import com.bikini.indexSave.mapper.ProductMapper;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class DocController {
	@Autowired(required=false)
	private TransportClient client;
	
	
	@Autowired
	private ProductMapper productMapper;
	@RequestMapping("index/add")
	public String addIndexDoc() throws Exception{
		/*Settings setting=Settings.builder()
				.put("cluster.name","elasticsearch").build();
		TransportClient client=new PreBuiltTransportClient(setting);
		InetSocketTransportAddress address= new InetSocketTransportAddress(
				InetAddress.getByName("10.9.104.184"),9300);
		client.addTransportAddress(address);*/
		//查询商品的list结果
		List<Product> pList = productMapper.selectAll();
		//循环遍历,添加数据document到es中
		ObjectMapper mapper=new ObjectMapper();
		for (Product product : pList) {
			//商品对象,解析json
			String json=mapper.writeValueAsString(product);
//			client.prepareIndex(index, type, id)
			client.prepareIndex("bikini", "product",
					product.getpId()).setSource(json).get();
		}
		return "success";
	}
}