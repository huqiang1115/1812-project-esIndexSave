package com.bikini.indexSave.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.bikini.common.pojo.Product;
@Mapper
public interface ProductMapper extends SystemMapper<Product>{

}
